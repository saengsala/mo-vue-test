import * as types from '../mutations/hello'
import axios from 'axios'

const state = {
    helloArray: [],
    currency: []
}

const getters = {
    getHelloArray: state => state.helloArray,
    getCurruency: state => state.currency
}

const actions = {
    fetchHelloArrayAction({ commit }) {
        const tempArray = ['ONE', 'TWO', 'THREE', 'FOUR', 'FIVE']
        commit(types.HELLO_ARRAY, tempArray)
    },
    async fetchCurrencyAction({ commit }) {
        const api = 'http://localhost:8081/api/currencies'
        let currencies = []
        await axios.get(api).then((response) => {
            if(response.data) {
                response.data.map( item => {
                     currencies.push({
                        code: item.code,
                        description: item.description
                    })
                })
            }
        })
        commit(types.CURRENCY_DATA, currencies)
    }

}

const mutations = {
    [types.HELLO_ARRAY](state, payload) {
      state.helloArray = payload
    },
    [types.CURRENCY_DATA](state, payload) {
        state.currency = payload
      }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
